export class CreateUserDto {
  data: {
    email: string;
    name: string;
  };
}
